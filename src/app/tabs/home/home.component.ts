import {Component, OnInit} from '@angular/core';
import {DatascopeComponent} from '../../shared/components/datascope/datascope.component';
import {ModalController} from '@ionic/angular';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

    widgets: any;
    header: any;


    constructor(private modalController: ModalController) {
        this.loadWidgets();
    }

    ngOnInit() {
        console.log('init');
    }

    loadWidgets() {
        this.header = JSON.parse(localStorage['home_header']);
        this.widgets = JSON.parse(localStorage['home']);
    }

    async dataScope() {
        const modal = await this.modalController.create({
            component: DatascopeComponent,
        });
        modal.onDidDismiss()
            .then(() => {
                this.loadWidgets();
            });
        return modal.present();

    }

}
