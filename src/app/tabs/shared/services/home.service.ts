import {Resolve, Router} from '@angular/router';
import {ModalController} from '@ionic/angular';
import {Injectable} from '@angular/core';
import {RequestService} from '../../../shared/services/request.service';
import {APP_URL} from '../../../shared/constants/url';
import {tap} from 'rxjs/operators';
import {HomeResponse} from '../../../shared/interfaces/response';
import {AccessDaniedComponent} from '../../../shared/components/access-danied/access-danied.component';

@Injectable()
export class HomeService implements Resolve<any> {


    constructor(public request: RequestService, public modalController: ModalController, public router: Router) {
    }

    resolve() {
    }

    get(data) {
        return this.request.get(APP_URL.homepage.get, data, false, true)
            .pipe(tap((res: HomeResponse) => {
                    localStorage['home'] = JSON.stringify(res.result);
                    localStorage['home_header'] = JSON.stringify(res.header);
                    localStorage['to_do'] = JSON.stringify(res['to-do']);
                },
                err => {
                    console.log(err);
                }));


    }

    datascope(type, data) {
        return this.request.get(APP_URL.datascope[type], data)
            .pipe(tap((res: HomeResponse) => {
                if (res.code === '1') {
                    this.presentModal({target: 'Data Scope'});
                    throw Error(res.message);
                }
                res.result.map(item => {
                    item.select = item.select === 'selected';
                });
            }));
    }

    scopeUpdate(data) {
        return this.request.post(APP_URL.datascope.update, data).pipe(tap((res) => {
            console.log(res);
        }));
    }

    async presentSelectModal(Component: any, prop?: any) {
        const modal = await this.modalController.create({
            component: Component,
            cssClass: 'selectModal',
            componentProps: {prop}
        });
        return await modal.present();
    }

    private async presentModal(prop?: any) {
        const modal = await this.modalController.create({
            component: AccessDaniedComponent,
            cssClass: 'shareModal',
            componentProps: prop
        });
        return await modal.present();
    }
}
