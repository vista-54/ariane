import {Resolve, Router} from '@angular/router';
import {APP_URL} from '../../../shared/constants/url';
import {Injectable} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {RequestService} from '../../../shared/services/request.service';
import {tap} from 'rxjs/operators';

declare interface SettingsResponse {
    code: string;
    message: string;
    result: any;
}

@Injectable()
export class SettingsService implements Resolve<any> {


    constructor(public request: RequestService, public modalController: ModalController, public router: Router) {
    }

    resolve() {
        const userId = JSON.parse(localStorage['user']).userid;
        return this.get({user_id: userId});
    }

    get(data) {
        return this.request.get(APP_URL.settings.get, data)
            .pipe(tap((res: SettingsResponse) => {
                    const settings = res.result.settings;
                    // Transform to boolean
                    settings.map(item => {
                        return item.value = item.value === '1' || item.value === 'true';
                    });
                },
                err => {
                    console.log(err);
                }));
    }

}
