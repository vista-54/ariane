import {Resolve, Router} from '@angular/router';
import {ModalController} from '@ionic/angular';
import {Injectable} from '@angular/core';
import {RequestService} from '../../../shared/services/request.service';
import {APP_URL} from '../../../shared/constants/url';
import {tap} from 'rxjs/operators';
import {AccessDaniedComponent} from '../../../shared/components/access-danied/access-danied.component';

@Injectable()
export class StoreVisitService implements Resolve<any> {


    constructor(public request: RequestService, public modalController: ModalController, public router: Router) {
    }

    resolve() {
        const userId = JSON.parse(localStorage['user']).userid;
        return this.get({user_id: userId});
    }

    get(data) {
        return this.request.get(APP_URL.store_visit.get, data)
            .pipe(tap((res: any) => {
                    console.log(res);
                    if (res.code === '1') {
                        this.presentModal({target: 'Store Visit'});
                        throw Error(res.message);
                    }
                },
                err => {
                    console.log(err);
                }));

    }

    scope(type, data) {
        return this.request.get(APP_URL.store_visit[type], data)
            .pipe(tap((res: any) => {
                if (res.code === '1') {
                    this.presentModal({target: 'store'});
                    throw Error(res.message);
                }
                res.result.map(item => {
                    item.select = item.select === 'selected';
                });
            }));
    }

    update(data) {
        return this.request.post(APP_URL.store_visit.update, data)
            .pipe(tap((res) => {
                console.log(res);
            }));
    }

    async presentSelectModal(Component: any, prop?: any) {
        const modal = await this.modalController.create({
            component: Component,
            cssClass: 'selectModal',
            componentProps: {prop}
        });
        return await modal.present();
    }

    private async presentModal(prop?: any) {
        const modal = await this.modalController.create({
            component: AccessDaniedComponent,
            cssClass: 'shareModal',
            componentProps: prop
        });
        return await modal.present();
    }

}
