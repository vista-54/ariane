import {Component} from '@angular/core';
import {CommonModalComponent} from '../commonModalComponent';
import {ModalController} from '@ionic/angular';

export declare const navigator;

@Component({
    selector: 'app-caution-modal-failed-retrieve-info',
    templateUrl: './caution-modal-failed-retrieve-info.component.html',
    styleUrls: ['./../modals.common.scss'],
})
export class CautionModalFailedRetrieveInfoComponent extends CommonModalComponent {

    constructor(modal: ModalController) {
        super(modal);
    }

    dismiss() {
        navigator['app'].exitApp();
    }
}
