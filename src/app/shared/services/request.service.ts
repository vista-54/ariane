import {Injectable} from '@angular/core';
import {Request} from '../interfaces/request.interface';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import * as queryString from 'query-string';
import {tap} from 'rxjs/operators';
import {ToastService} from './toast.service';
import {LoadingController, ModalController} from '@ionic/angular';
import {LoadingService} from './loading.service';
import {DataPreparationComponent} from '../components/data-preparation/data-preparation.component';

@Injectable()
export class RequestService implements Request {

    // loading;

    constructor(private http: HttpClient,
                private router: Router,
                private loading: LoadingService,
                private toastService: ToastService,
                private modalCtrl: ModalController,
                private loadingController: LoadingController) {
    }

    public get(url: string, body: object = null, loader: boolean = true, modal: boolean = false) {
        if (loader) {
            this.loading.present();
        }
        if (modal) {
            this.presentModal(DataPreparationComponent);
        }
        if (body !== null) {
            if (Object.keys(body).length > 0) {
                url += '?' + queryString.stringify(body);
            }
        }
        return this.http.get(url)
            .pipe(tap(() => {
                if (loader) this.loading.dismiss();
                if (modal) {
                    this.modalCtrl.dismiss({
                        dismissed: true
                    });
                }

            }, error => {
                if (!error.status) {
                    this.toastService.toast('danger', error.statusText);
                } else {
                    this.toastService.toast('danger', error);
                }
                if (loader) this.loading.dismiss();

                if (modal) {
                    this.modalCtrl.dismiss({
                        dismissed: true
                    });
                }
            }))
            ;
    }

    public post(url: string, credentials: any, loader: boolean = true) {
        if (loader) this.loading.present();
        return this.http.post(url, credentials)
            .pipe(tap(() => {
                if (loader) this.loading.dismiss();
            }, error => {
                this.toastService.toast('danger', error);
            }));
    }

    public put(url: string, credentials: any, loader: boolean = true) {
        if (loader) this.loading.present();
        return this.http.put(url, credentials)
            .pipe(tap(() => {
                if (loader) this.loading.dismiss();
            }, error => {
                this.toastService.toast('danger', error);
            }));
    }

    public delete(url: string, data: object = null, loader: boolean = true) {
        if (loader) this.loading.present();
        return this.http.delete(url)
            .pipe(tap(() => {
                if (loader) this.loading.dismiss();
                this.toastService.toast('success', 'Данные успешно удалены');
            }, error => {
                this.toastService.toast('danger', error);
            }));
    }

    public getFile(url: string, body: object = null, loader: boolean = true) {
        if (loader) this.loading.present();
        if (body !== null) {
            if (Object.keys(body).length > 0) {
                url += '?' + queryString.stringify(body);
            }
        }
        return this.http.get(url, {responseType: 'text'})
            .pipe(tap(() => {
                if (loader) this.loading.dismiss();
            }, error => {
                this.toastService.toast('danger', error);
            }));
    }

    private async presentModal(Component: any) {
        const modal = await this.modalCtrl.create({
            component: Component
        });
        return await modal.present();
    }

}
