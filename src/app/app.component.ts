import {AfterViewInit, Component, NgZone} from '@angular/core';

import {NavController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {TranslateService} from '@ngx-translate/core';
import {NavigationExtras, Router} from '@angular/router';

declare const universalLinks;

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements AfterViewInit {

    constructor(private platform: Platform,
                private splashScreen: SplashScreen,
                private statusBar: StatusBar,
                public translate: TranslateService,
                private router: Router,
                private navCtrl: NavController, private ngZone: NgZone) {

        this.initializeApp();

    }

    initializeApp() {
        // const navigationExtras: NavigationExtras = {
        //     state: {
        //         email: 'vista545457@gmail.com',
        //         supplier_id: '0059',
        //     }
        // };
        // this.router.navigate(['/sdk'], navigationExtras);
        console.log('inited')
        this.translate.setDefaultLang('en');
        this.translate.use('en');
        const that = this;
        this.platform.ready().then(() => {
            universalLinks.subscribe('sdkEvent', (res) => {
                if (res.path.indexOf('sdk') !== -1 && res.hasOwnProperty('params')) {
                    if (res.params.hasOwnProperty('email') && res.params.hasOwnProperty('supplier_id')) {
                        const navigationExtras: NavigationExtras = {
                            state: {
                                email: res.params.email,
                                supplier_id: res.params.supplier_id,
                            }
                        };
                        console.warn(NgZone.isInAngularZone()); // false
                        this.ngZone.run(() => {
                            console.warn(NgZone.isInAngularZone()); // true
                            this.router.navigate(['/sdk'], navigationExtras);
                        });
                    }
                    console.log(res);
                }


            });

            this.splashScreen.hide();
        });
    }


    ngAfterViewInit() {
        this.platform.ready().then(() => {
            this.splashScreen.hide();

        });
    }
}
