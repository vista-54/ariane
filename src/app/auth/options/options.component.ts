import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../shared/services/auth.service';
import {RESPONSE_CODE, RESPONSE_CODE_SETTINGS_UPDATE} from '../../shared/constants/response_code';
import {HomeResponse, SettingsUpdateResponse} from '../../shared/interfaces/response';
import {HomeService} from '../../tabs/shared/services/home.service';

@Component({
    selector: 'app-options',
    templateUrl: './options.component.html',
    styleUrls: ['./options.component.scss'],
})
export class OptionsComponent implements OnInit {

    settings: any;
    user: any;

    constructor(private router: Router,
                private auth: AuthService,
                private home: HomeService) {
        this.settings = JSON.parse(localStorage.settings);
        this.user = JSON.parse(localStorage.user);
    }

    ngOnInit() {
    }

    next() {
        this.auth.settingsUpdate({
            user_id: this.user.userid,
            setting: this.settings
        })
            .subscribe(success => {
                const result = success as SettingsUpdateResponse;
                if (result.code === RESPONSE_CODE_SETTINGS_UPDATE.SUCCESS) {
                    this.loadHome();
                }
            });
    }

    loadHome() {
        this.home.get({user_id: this.user.userid}).subscribe(success => {
            const result = success as HomeResponse;
            if (result.code === RESPONSE_CODE.SUCCESS) {
                this.router.navigate(['auth/well-done']);
            }
        });
    }
}
