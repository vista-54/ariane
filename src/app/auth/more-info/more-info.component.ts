import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {AuthService} from '../shared/services/auth.service';
import {Router} from '@angular/router';
import {RESPONSE_CODE_VERIFY_TOKEN} from '../../shared/constants/response_code';
import {VerifyTokenResponse} from '../../shared/interfaces/response';
import {Events, IonInput} from '@ionic/angular';

@Component({
    selector: 'app-more-info',
    templateUrl: './more-info.component.html',
    styleUrls: ['./more-info.component.scss'],
})
export class MoreInfoComponent implements OnInit {
    credentials: any;
    user: any;
    @ViewChild('code', {static: false}) inputElement: IonInput;

    constructor(private auth: AuthService,
                private formBuilder: FormBuilder,
                public router: Router,
                public events: Events) {
        this.user = JSON.parse(localStorage.user);
        this.credentials = this.formBuilder.group({
            email: [this.user.email, [Validators.required]],
            supplier_id: [this.user.supplier_id, [Validators.required]],
            company: [this.user.company, [Validators.required]],
            firstname: [this.user.firstname, [Validators.required]],
            lastname: [this.user.lastname, [Validators.required]],
            position: [this.user.position, []],
            subscription: [this.user.subscription, []],
            phone: [this.user.phone, []],
            token: ['', [Validators.required]],
        });
        events.subscribe('token:error', () => {
            this.inputElement.setFocus();
        });
    }

    ngOnInit() {
    }

    next() {
        this.auth.verifyToken({
            user_id: this.user.userid,
            token: this.credentials.value.token
        })
            .subscribe(success => {
                const result = success as VerifyTokenResponse;
                if (result.code === RESPONSE_CODE_VERIFY_TOKEN.SUCCESS) {
                    this.router.navigate(['auth/options']);
                }
            });
    }

    requestToken() {
        this.auth.requestToken({user_id: this.user.userid})
            .subscribe(success => {
                console.log(success);
            });
    }
}
