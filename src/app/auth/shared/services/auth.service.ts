import {APP_URL} from '../../../shared/constants/url';
import {Injectable} from '@angular/core';
import {RequestService} from '../../../shared/services/request.service';
import {tap} from 'rxjs/operators';
import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
import {ModalController} from '@ionic/angular';
import {CautionModalMismatchComponent} from '../../../shared/components/caution-modal-mismatch/caution-modal-mismatch.component';
import {CautionModalExpiredComponent} from '../../../shared/components/caution-modal-expired/caution-modal-expired.component';
import {RESPONSE_CODE_LOGIN, RESPONSE_CODE_VERIFY_TOKEN} from '../../../shared/constants/response_code';
import {TokenSentComponent} from '../../../shared/components/token-sent/token-sent.component';
import {RegistrationSuccessComponent} from '../../../shared/components/registration-success/registration-success.component';
import {LoginResponse, RegisterResponse, RequestTokenResponse, VerifyTokenResponse} from '../../../shared/interfaces/response';
import {UnregisteredModalComponent} from '../../../shared/components/unregistered-modal/unregistered-modal.component';
import {CautionModalMissedComponent} from '../../../shared/components/caution-modal-missed/caution-modal-missed.component';
import {CautionModalThirtyInactiveComponent} from '../../../shared/components/caution-modal-thirty-inactive/caution-modal-thirty-inactive.component';
import {CautionModalNoMobileComponent} from '../../../shared/components/caution-modal-no-mobile/caution-modal-no-mobile.component';
import {CautionModalFailedRetrieveInfoComponent} from '../../../shared/components/caution-modal-failed-retrieve-info/caution-modal-failed-retrieve-info.component';
import {CautionInvalidTokenComponent} from '../../../shared/components/caution-invalid-token/caution-invalid-token.component';
import {CautionInvalidTokenMultitimeComponent} from '../../../shared/components/caution-invalid-token-multitime/caution-invalid-token-multitime.component';
import {ModalReactivationSuccessComponent} from '../../../shared/components/modal-reactivation-success/modal-reactivation-success.component';


@Injectable()
export class AuthService implements Resolve<any> {


    private errorCounterLogin = 0;
    private errorCounterToken = 0;

    constructor(public request: RequestService, public modalController: ModalController,
                public router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot) {
        const userId = route.queryParams['user_id'];
        if (userId) {
            return this.getUserInfoById(userId);
        }
    }

    getUserInfoById(userId) {
        return this.request.get(APP_URL.user.get, {user_id: userId})
            .pipe(tap(res => {
                const result = res as LoginResponse;
            }));
    }

    login(data: any) {
        return this.request.post(APP_URL.auth.login, data)
            .pipe(tap(res => {
                const result = res as LoginResponse;
                switch (result.code) {
                    case RESPONSE_CODE_LOGIN.SUCCESS:
                        result.info.email = data.email;
                        localStorage['user'] = JSON.stringify(result.info);
                        localStorage['jwt'] = result.jwt;
                        break;
                    case RESPONSE_CODE_LOGIN.UNREGISTERED_SUPPLIER:
                        this.presentModal(UnregisteredModalComponent);
                        break;
                    case RESPONSE_CODE_LOGIN.CREDENTIALS_MISMATCH:
                        this.errorCounterLogin++;
                        this.presentModal(CautionModalMismatchComponent, {counter: this.errorCounterLogin});
                        break;
                    case RESPONSE_CODE_LOGIN.MISSED_REQUIRED_INFO:
                        this.presentModal(CautionModalMissedComponent);
                        break;
                    case RESPONSE_CODE_LOGIN.THIRTY_DAYS_INACTIVE:
                        this.presentModal(CautionModalThirtyInactiveComponent, {userid: result.userid});
                        break;
                    case RESPONSE_CODE_LOGIN.NO_MOBILE_ACCESS:
                        this.presentModal(CautionModalNoMobileComponent);
                        break;
                    case RESPONSE_CODE_LOGIN.FAILED_RETRIEVE_INFO:
                        this.presentModal(CautionModalFailedRetrieveInfoComponent);
                        break;
                    case RESPONSE_CODE_LOGIN.SUBSCRIPTION_EXPIRED:
                        this.presentModal(CautionModalExpiredComponent);
                        break;
                }
            }, err => {
            }));
    }

    requestToken(data) {
        return this.request.post(APP_URL.auth.request_token, data)
            .pipe(tap(res => {
                const result = res as RequestTokenResponse;
                switch (result.code) {
                    case '0':
                        this.presentModal(TokenSentComponent);
                        break;
                }
            }));
    }

    verifyToken(data) {
        return this.request.post(APP_URL.auth.verify, data)
            .pipe(tap(res => {
                const result = res as VerifyTokenResponse;
                switch (result.code) {
                    case '0':
                        localStorage['settings'] = JSON.stringify(result.settings);
                        break;
                    case RESPONSE_CODE_VERIFY_TOKEN.TOKEN_EXPIRED:
                        this.errorCounterToken++;
                        if (this.errorCounterToken === 3) {
                            this.presentModal(CautionInvalidTokenMultitimeComponent);
                        } else {
                            this.presentModal(CautionInvalidTokenComponent);
                        }
                        break;
                }
            }));
    }

    settingsUpdate(data) {
        return this.request.post(APP_URL.auth.settings_update, data)
            .pipe(tap(res => {
                console.log(res);
            }));
    }

    register(data) {
        return this.request.post(APP_URL.auth.register, data)
            .pipe(tap(res => {
                const result = res as RegisterResponse;
                switch (result.code) {
                    case '0':
                        this.presentModal(RegistrationSuccessComponent);
                        break;
                }
            }));
    }

    reactivate(data) {
        return this.request.post(APP_URL.auth.reactivate, data)
            .pipe(tap(res => {
                const result = res as RegisterResponse;
                switch (result.code) {
                    case '0':
                        this.presentModal(ModalReactivationSuccessComponent);
                        break;
                    case'6':
                        break;
                }
            }));
    }

    private async presentModal(Component: any, prop?: any) {
        const modal = await this.modalController.create({
            component: Component,
            cssClass: 'shareModal',
            componentProps: prop
        });
        return await modal.present();
    }
}
